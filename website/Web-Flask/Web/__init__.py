from flask import Flask, render_template, flash, request, url_for, redirect
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from dbconnection import connection
from mongodb_connect import Hashtags, Trend_Topics, Connect, Golden_Set, Learned_Set, Learned_Set2, Golden_Set2, Not_Related_Set, Not_Related_Set2
from flask import Blueprint
from flask_paginate import Pagination
from tweepy import OAuthHandler
from tweepy import Stream
import configparser
import tweepy
from tweepy.streaming import StreamListener
import json
import numpy as np
import ast
import datetime
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.plotting import figure, show, output_file
from bokeh.embed import components
from bokeh.plotting import *
import pandas as pd
import pandas as pd
from numpy.random import randint
import datetime as dt
import matplotlib.pyplot as plt
from flask_googlemaps import GoogleMaps
from flask_googlemaps import Map


mod = Blueprint('', __name__)

consumer_key="HfiW5o8jexvJszUd4Y3jIkbMm"
consumer_secret="QxTe7fvQvT3ULlNmDqXyQliBTdWhO0R6t3ANeK4Ny474YPqZNn"
access_token="706599292102975490-INsyV9RzacDecy4vo9I3fuT1rfxUeyX"
access_token_secret="cRbk6aMdpYgyHkgzxdc0wnGrL1KMEZwtDzkBnBeleU8yq"



Trend_Topics = Trend_Topics()
All_Tweets = Hashtags()
localcol = Connect()
app = Flask(__name__)
app.config['GOOGLEMAPS_KEY'] = 'AIzaSyDLzH5T_F3HJGYecetZVXALKcPv0qCWyLU'

GoogleMaps(app, key='AIzaSyDaJ0-Kl9sM9GO9JPy1demDgAjsB2EiFrg')
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

class ReusableForm(Form):
    name = TextField('Ara:', validators=[validators.required()])


@app.route('/search/<key_word>')
def search(key_word):
    golden_set2 = Golden_Set2()
    learned_set2 = Learned_Set2()
    form = ReusableForm(request.form)
    tweets = Hashtags()
    search = False
    q = request.args.get('q')

    if q:
        search = True
    page = request.args.get('page', type=int, default=1)
    arr = []

    for tweet in tweets:
        name = key_word
        if name in str(tweet['text']):
            start_index = tweet['text'].index(name)
            #print(start_index)
            length = len(name)
            end_index = start_index + length
            tweet['start'] = start_index
            tweet['end'] = end_index
            arr.append(tweet)

    total = len(arr)
    if request.method == 'POST':
        option = request.form.get('dropdown')
        print(option)
        try:
            option = option.split(':')
            set_name = option[0]
            tweet_id = option[1]
            print(set_name +'\t'+ tweet_id)
            if set_name == 'Related':
                twe = learned_set2.find({"_id" : str(tweet_id)})
                print('twe', twe[0])
                golden_set2.insert(twe)

            elif set_name == 'NotRelated':
                pass
            else:
                pass
        except:
            print('operation failed!')
        return render_template("main.html")
    arr = arr[(page * 10 - 10): (page * 10)]
    pagination = Pagination(page=page, total=total, search=search, record_name='')
    return render_template("search.html", all_tweets=All_Tweets, trend_topics=Trend_Topics, form=form, searched=arr,pagination=pagination)


@app.route("/", methods=['GET', 'POST'])
def homepage():
    return render_template("main.html", all_tweets=All_Tweets, trend_topics=Trend_Topics)

@app.route("/map/", methods=['GET', 'POST'])
def map():
    return render_template("map.html", all_tweets=All_Tweets, trend_topics=Trend_Topics)

@app.route("/trend_topics/", methods=['GET', 'POST'])
def trend_topics():
    form = ReusableForm(request.form)
    search = False
    q = request.args.get('q')
    if q:
        search = True

    page = request.args.get('page', type=int, default=1)
    hashtags = Trend_Topics[(page * 17 - 17):(page * 17)]
    pagination = Pagination(page=page, total=90, search=search, record_name='')

    if request.method == 'POST':
        form = ReusableForm(request.form)

        name=request.form['name']
        if form.validate():
            # Save the comment here.
            flash('Hello ' + name)
        else:
            flash('All the form fields are required. ')

        return redirect("/search/" + name)

    return render_template("trend_topics.html",pagination=pagination, all_tweets=All_Tweets, trend_topics=hashtags, form=form)
@app.route("/learned_tweets/<tweet_set>/", methods=['GET', 'POST'])
def learned_page(tweet_set):
    golden_set2 = Golden_Set2()
    learned_set2 = Learned_Set2()
    not_related_set2 = Not_Related_Set2()
    search = False
    q = request.args.get('q')
    if q:
        search = True

    page = request.args.get('page', type=int, default=1)
    print('page', page)
    print('q', q)
    all_tweets = []

    if tweet_set == 'golden_set':
        print(tweet_set)
        all_tweets = Golden_Set()

    elif tweet_set == 'not_related_set':
        print(tweet_set)
        all_tweets = Not_Related_Set()

        
    elif tweet_set == 'learned_set':
        all_tweets = Learned_Set()

    total = all_tweets.count()
    print('total', total)
    print('all', all_tweets)
    all_tweets = all_tweets[(page*10-10):(page*10)]
    pagination = Pagination(page=page, total=total, search=search, record_name='')
    if request.method == 'POST':
        option = request.form.get('dropdown')

        print('1',str(option))
        try:
            option = option.split(':')
            set_name = option[0]
            sett = set_name + ':'
            tweet_id = ':'.join(option).replace(sett, '')
            print(set_name +'\t'+ tweet_id)
            if set_name == 'Related':
                if tweet_set == 'not_related_set':
                   twe = not_related_set2.find({"_id" : ast.literal_eval(tweet_id)})
                   print('twe', twe[0])
                   golden_set2.insert(twe)
                   not_related_set2.remove({"_id": ast.literal_eval(tweet_id)})
                elif tweet_set == 'learned_set':
                    twe = learned_set2.find({"_id": ast.literal_eval(tweet_id)})
                    print('twe', twe[0])
                    golden_set2.insert(twe)
                    learned_set2.remove({"_id": ast.literal_eval(tweet_id)})

            elif set_name == 'NotRelated':
                if tweet_set == 'golden_set':
                     twe = golden_set2.find({"_id": ast.literal_eval(tweet_id)})
                     print('twe', twe[0])
                     not_related_set2.insert(twe)
                     golden_set2.remove({"_id": ast.literal_eval(tweet_id)})
                elif tweet_set == 'learned_set':
                     twe = learned_set2.find({"_id": ast.literal_eval(tweet_id)})
                     print('twe', twe[0])
                     not_related_set2.insert(twe)
                     learned_set2.remove({"_id": ast.literal_eval(tweet_id)})

            elif set_name == 'Delete':
                if tweet_set == 'golden_set':
                    twe = golden_set2.find({"_id": ast.literal_eval(tweet_id)})
                    golden_set2.remove({"_id": ast.literal_eval(tweet_id)})
                elif tweet_set == 'learned_set':
                    twe = learned_set2.find({"_id": ast.literal_eval(tweet_id)})
                    learned_set2.remove({"_id": ast.literal_eval(tweet_id)})
                elif tweet_set == 'not_related_set':
                    twe = not_related_set2.find({"_id": ast.literal_eval(tweet_id)})
                    not_related_set2.remove({"_id": ast.literal_eval(tweet_id)})
        except:
            print('operation failed!')
    return render_template("learned_page.html", pagination=pagination, users=all_tweets, key_word=str(tweet_set))


@app.route('/trend_topic/<hashtag>')
def trend_topic(hashtag):
    all_tweets = localcol.find({'hashtag':str(hashtag)})
    arr = []
    search = False
    q = request.args.get('q')
    if q:
        search = True

    page = request.args.get('page', type=int, default=1)
    for tweet in all_tweets:

        arr.append(tweet)
    length = len(Trend_Topics)
    hashtags = Trend_Topics[(page * 17 - 17):(page * 17)]
    pagination = Pagination(page=page, total=length, search=search, record_name='')
    return render_template("trend_topic.html", all_tweets=arr,trend_topics=hashtags, pagination=pagination, length=len(hashtags))



@app.route('/grafics/')
def grafics():

    """
    kws = ['1','2','3','4','5','6','7','8','9']
    df1 = [18336, 23141, 22403, 20889, 23259, 18512, 20040, 18857, 23883]
    df = pd.DataFrame({'kw': kws,'df1': df1})
    df.set_index("kw", inplace=True)
    series = df["df1"]
    p = figure(width=800, height=400, y_range=series.index.tolist())
    j = 1
    for k, v in series.iteritems():
        w = v / 2 * 2
        p.rect(x=v / 2,
               y=j,
               width=w,
               height=0.4,
               color=(76, 114, 176),
               width_units="screen",
               height_units="screen"
               )
        j += 1"""
    fig = figure(title="Günlük Tweet Sayısı",width=800, height=400)
    df1 = [18336, 23141, 22403, 20889, 23259, 18512, 20040, 18857, 23883]
    dates = ['28 Aralık','29 Aralık','30 Aralık','31 Aralık','1 Ocak','2 Ocak','3 Ocak','4 Ocak','5 Ocak']
    x = ['Mon Sep 1 16:40:20 2015', 'Mon Sep 1 16:45:20 2015',
         'Mon Sep 1 16:50:20 2015', 'Mon Sep 1 16:55:20 2015', 'Mon Sep 1 16:40:20 2015', 'Mon Sep 1 16:45:20 2015',
         'Mon Sep 1 16:50:20 2015', 'Mon Sep 1 16:55:20 2015', 'Mon Sep 1 16:55:20 2015']

    x = [datetime.datetime.strptime(elem, '%a %b %d %H:%M:%S %Y') for elem in x]
    fig.line(pd.date_range('12/28/2016', '1/05/2017', freq='D'), df1)

    script, div = components(fig)

    page = render_template('grafics.html', div=div, script=script)
    return page

if __name__ == "__main__":
    app.secret_key = 'secretscrt'
    app.debug = True
    app.run(debug=True,port=5000)

