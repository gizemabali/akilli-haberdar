from flask import Flask
from flask.ext.pymongo import PyMongo
import json
import pymongo
from pymongo import MongoClient


def Connect():
       db_host = 'localhost'
       db_port = 27017
       dbname = 'hashtags'
       collname = 'hashtags'

       try:
          client = MongoClient(db_host, db_port)
          localdb = client[dbname]
          localcol = localdb[collname]
          print("MongoDB database : " + dbname + ", collection : " + collname + ")")

       except:
          print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")

       return localcol
        

def Hashtags():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'tweet_data'

    try:
          client = MongoClient(db_host, db_port)
          localdb = client[dbname]
          localcol = localdb[collname]
          print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
          print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
    posts = localcol.find()
    
    return posts

def Trend_Topics():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'hashtags'

    try:
          client = MongoClient(db_host, db_port)
          localdb = client[dbname]
          localcol = localdb[collname]
          print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
          print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
    
    posts = localcol.find({'all':'all'})
    post = []
    for i in posts:
        post = i['counts']
    return post
#localcol.insert({'Gizem':'1'})

def Golden_Set():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'golden_set'

    try:
          client = MongoClient(db_host, db_port)
          localdb = client[dbname]
          localcol = localdb[collname]
          print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
          print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
    
    golden = localcol.find()
    return golden

def Learned_Set():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'learned_set'

    try:
          client = MongoClient(db_host, db_port)
          localdb = client[dbname]
          localcol = localdb[collname]
          print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
          print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
    
    learned = localcol.find({'type':'related'})
    return learned


def Learned_Set2():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'learned_set'

    try:
        client = MongoClient(db_host, db_port)
        localdb = client[dbname]
        localcol = localdb[collname]
        print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
        print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")

    learned = localcol.find()
    return localcol


def Golden_Set2():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'golden_set'

    try:
        client = MongoClient(db_host, db_port)
        localdb = client[dbname]
        localcol = localdb[collname]
        print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
        print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")


    return localcol


def Not_Related_Set2():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'not_related_set'

    try:
        client = MongoClient(db_host, db_port)
        localdb = client[dbname]
        localcol = localdb[collname]
        print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
        print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")


    return localcol

def Not_Related_Set():
    db_host = 'localhost'
    db_port = 27017
    dbname = 'hashtags'
    collname = 'not_related_set'

    try:
        client = MongoClient(db_host, db_port)
        localdb = client[dbname]
        localcol = localdb[collname]
        print("MongoDB database : " + dbname + ", collection : " + collname + ")")

    except:
        print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")

    learned = localcol.find()
    return learned