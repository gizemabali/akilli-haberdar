#!/usr/bin/env python
import sys
import os
from setuptools import setup, find_packages

setup(name='Distutils',
      version='1.0',
      description='Name Entity Recognition',
      author='Gizem Abalı',
      author_email='gizemabali93@gmail.com',
      packages= find_packages(where = 'ner'),
      package_dir = {'':'ner'},
     )
