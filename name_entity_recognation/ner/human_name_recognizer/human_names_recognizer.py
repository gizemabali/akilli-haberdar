#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import os
import pandas as pd
import numpy as np
import statistics as s
import string
import time
import itertools
import codecs
import sys
sys.path.append('/home/hp/Masaüstü/analysing_aegean_tweets/name_entity_recognation/ner/human_name_recognizer/')
#from datautilsmaster.datautils.unicode_tr import unicode_tr
#import pyparsing as pp
from pyparsing import Word, alphas, nums, WordStart, WordEnd, Literal, Optional, SkipTo, ParseResults, CaselessLiteral, ParserElement, \
        OneOrMore, Group, White, ZeroOrMore, NotAny, Suppress, oneOf, Combine, Regex, lineStart, Empty, locatedExpr,\
        ParseException, LineStart, FollowedBy

class HumanNames():
        def __init__(self):
            
            self.first_names = []
            self.surnames_list = []
            self.names = ''
            self.surnames = ''
            with open(os.path.dirname(__file__) +"/TR_human_names/names.txt", 'r') as file :
                  for f in file:
                      f = f.lower()
                      f = f.replace('\n', '')
                      f = f.replace('i̇', 'i')
                      f = f.replace('i̇', 'i')
                      self.first_names.append(f)

            with open(os.path.dirname(__file__)+"/TR_human_names/surnames.txt", 'r') as file :
                  for f in file:
                      f = f.lower()
                      f = f.replace('\n', '')
                      f = f.replace('i̇', 'i')
                      f = f.replace('i̇', 'i')
                      self.surnames_list.append(f)

            self.names = self.get_regex(self.first_names)
            self.surnames = self.get_surregex(self.surnames_list)
        def regex_firstnames(self, first_names):
             regex_names = ''
             for index,name in enumerate(first_names):
                 if len(name)>0:
                    if name[0] == "'":
                        regex_names += name + '|\\b'
                    else:
                        if index == 0: # do not allow duplicate
                             regex_names += name
                        else:
                             regex_names += "|\\b" + name
             regex_names = regex_names
             return regex_names 

        def regex_surnames(self, surnames):
             regex_names = ''
             for index,name in enumerate(surnames):
                 if len(name)>0:
                    if name[0] == "'":
                        regex_names += name + '|'
                    else:
                        if index == 0: # do not allow duplicate
                             regex_names += name
                        else:
                             regex_names += "|" + name
             regex_names = regex_names
             return regex_names

        def sort_names(self, names):
             names = list(set(names))
             names = sorted(names, key=len, reverse=True)
             names = [tx.replace('.','\.') for tx in names] # not to match anything with . (dot)
             names = [tx.replace("(","\(") for tx in names] # not to match anything with . (dot)
             names = [tx.replace(")","\)") for tx in names] # not to match anything with . (dot)
             return names
                  
        def get_regex(self, mylist):
             names = self.sort_names(mylist)
             return Combine(Regex(re.compile(self.regex_firstnames(names), re.I)))

        def get_surregex(self, mylist):
             names = self.sort_names(mylist)
             return Combine(Regex(re.compile(self.regex_surnames(names), re.I)))

        def construct_location_grammar(self):
            first_name_pttrn = self.names.setResultsName("firstname")
            surname_pttrn = self.surnames.setResultsName("surname")
            full_name_pttrn = Combine(self.names + ' ' + self.surnames).setResultsName("full human name")
            full_name_pttrn2 = Combine(self.names + self.surnames).setResultsName("full human name2")
            def first_name_pttrn_trnsfrm(loc,toks):
                 return {"firstname":" ".join(toks)}
            def full_name_pttrn_trnsfrm(loc,toks):
                 return {"full human name":" ".join(toks)}
            first_name_pttrn.setParseAction(first_name_pttrn_trnsfrm)
            full_name_pttrn.setParseAction(full_name_pttrn_trnsfrm)
            full_name_pttrn2.setParseAction(full_name_pttrn_trnsfrm)
            name_pttrn = full_name_pttrn | full_name_pttrn2
            return name_pttrn
