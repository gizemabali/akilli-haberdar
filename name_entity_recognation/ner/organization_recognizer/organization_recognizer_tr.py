#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import os
import pandas as pd
import numpy as np
import statistics as s
import string
import time
import sys
import itertools
import codecs
sys.path.append('/home/hp/Masaüstü/analysing_aegean_tweets/name_entity_recognation/ner/organization_recognizer/')
# from datautilsmaster.datautils.unicode_tr import unicode_tr
# import pyparsing as pp
from pyparsing import Word, alphas, nums, WordStart, WordEnd, Literal, Optional, SkipTo, ParseResults, CaselessLiteral, \
    ParserElement, \
    OneOrMore, Group, White, ZeroOrMore, NotAny, Suppress, oneOf, Combine, Regex, lineStart, Empty, locatedExpr, \
    ParseException, LineStart, FollowedBy


class OrganizationNames():
    def __init__(self):


        self.organization_list = []
        self.organization_names = ''

        with open(os.path.dirname(__file__) +"/TR_organizations/organization_names.txt", 'r') as file:
            for f in file:
                f = f.lower()
                f = f.replace('\n', '')
                f = f.replace('i̇', 'i')
                f = f.replace('i̇', 'i')
                self.organization_list.append(f)

        self.organization_names = self.get_regex(self.organization_list)


    def regex_organizations(self, names):
        regex_names = ''
        for index, name in enumerate(names):
            if len(name) > 0:
                if name[0] == "'":
                    regex_names += name + '|\\b'
                else:
                    if index == 0:  # do not allow duplicate
                        regex_names += name
                    else:
                        regex_names += "|\\b" + name
        regex_names = regex_names
        return regex_names



    def sort_names(self, names):
        names = list(set(names))
        names = sorted(names, key=len, reverse=True)
        names = [tx.replace('.', '\.') for tx in names]  # not to match anything with . (dot)
        names = [tx.replace("(", "\(") for tx in names]  # not to match anything with . (dot)
        names = [tx.replace(")", "\)") for tx in names]  # not to match anything with . (dot)
        return names

    def get_regex(self, mylist):
        names = self.sort_names(mylist)
        return Combine(Regex(re.compile(self.regex_organizations(names), re.I)))


    def construct_location_grammar(self):
        organization_name_pttrn = self.organization_names.setResultsName("names")


        def organization_name_pttrn_trnsfrm(loc, toks):
            return {"organization name": " ".join(toks)}

        organization_name_pttrn.setParseAction(organization_name_pttrn_trnsfrm)

        name_pttrn = organization_name_pttrn
        return name_pttrn
