#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import os
import pandas as pd
import numpy as np
import statistics as s
import string
import time
import itertools
import codecs

#from datautilsmaster.datautils.unicode_tr import unicode_tr
#import pyparsing as pp
from pyparsing import Word, alphas, nums, WordStart, WordEnd, Literal, Optional, SkipTo, ParseResults, CaselessLiteral, ParserElement, \
        OneOrMore, Group, White, ZeroOrMore, NotAny, Suppress, oneOf, Combine, Regex, lineStart, Empty, locatedExpr,\
        ParseException, LineStart, FollowedBy
ParserElement.enablePackrat()

hsnw = open("wgizem.txt","w")
class PlaceNames():
         
         def __init__(self, geonames_file="TR_Geonames/TR.txt"):
                self.start = time.time()
                self.script_dir = os.path.dirname(__file__)+"/"
                self.geonames_file = self.script_dir+geonames_file
                #-----
                # create two arrays to divide place names according to the length
                self.short_placelist = []
                self.long_placelist = []
                self.not_p_names_s = ['Doga', 'Dıra', 'Arac', 'Sali', 'Kar', 'Bey', 'ince', 'Ocak', 'Saat','Gar','Beni','Seni','Algı','İnce'\
                    'Eser','Tan', 'Lar','Kan', 'Dolu', 'Eski', 'Has','Kola','Dimi','Ulus','Yeni','Son','Top','Cuma','salı',\
                    'Olur','Zor','Dur', 'İnci', 'Bas','Yazı','Sırt', 'İmam', 'Yalı','Öncü','İçme', 'İdil', 'İkiz']

                self.not_p_names_l = ['Kaygısız','Ayrım','Ertuğrul','Yoğun','Dozer','Termal','Kaldırım','Yardimci','Kapali','İsmail', \
                     'Kapalı','Konus','Valilik', 'Yogun', 'Yazlık', 'Santral', 'Polis', 'Pazar', 'Maden', 'Orman', 'Giden', 'Nokta', \
                     'Meydan', 'Merkez','Elektrik', 'İstasyon', 'Istasyon', 'Tarim','sakallı', 'Devlet', 'Sakallı', 'İlhan', 'Gürün',  \
                     'Turunc', 'Sevimli','Zeytin','Kaldı','Kaldi','İkili','İşaret','Tatili','İleri', 'Ayvalık', 'Ayvalik', 'Tatılı', \
                     'Cennet','Destek','Bilgi','yardımcı', 'tarım','Ikinci','ikinci','Olacak','Toprak','Ziraat','Baraj','Adalet',\
                     'İslam', 'Tatili','tatili','Ayrım', 'ayrım','ardınc','ileri','aşağı','İkinci', 'İsmet', 'İnönü', 'Tatili']
                self.appendix_with_front_vowel =["den", "de", "ye","yi","yü","i", "e","ö","ü","nin", "nün","in","ün","ymiş",\
                      "ymüş","miş", "müş","ydi", "ydü","di","dü","dan", "da", "ya", "yı", "yu", "ı", "u", "a", "o", "nın","ın", "nun", \
                      "un", "ymış","ymuş","mış","muş","ydı","ydu","dı","du"]
                self.appendix_with_back_vowels = ["dan", "da", "ya", "yı", "yu", "ı", "u", "a", "o", "nın","ın", "nun", \
                      "un", "ymış","ymuş","mış","muş","ydı","ydu","dı","du"] 
                self.location_supporters = ["köyü","köy","ilçesi","ilçe", "gölü", "göl", "mahallesi", "mahalle", "mah", "yolu", "yol" ,\
                   "önü", "kavşağı", "kavşak", "caddesi", "cadde", "cad", "sokağı", "sokak", "sok", "bulvarı", "bulvar", "oteli", \
                   "otel", "camisi", "cami", "koyu", "vadisi", "vadi", "ovası", "barajı", "nehri", "merkezi", "merkez", "meydanı",\
                   "meydan", "parkı", "park", "belediyesi", "deresi", "sitesi", "bölgesi", "hastanesi", "hastane", \
                   "Üniversitesi", "Üniversite", "havaalanı", "durak", "durağı", "Durağı", "çıkışı", "girişi"]
                self.appendix_with_front_vowel_pttrn = self.appendix_regex_front(self.appendix_with_front_vowel)
                self.appendix_with_back_vowel_pttrn = self.appendix_regex_back(self.appendix_with_back_vowels)
                self.appendix_pattern = re.compile(self.appendix_with_front_vowel_pttrn )
                self.long_place_names_wt_space = []
                self.location_supporter_pattern = self.location_supporter_regex(self.location_supporters)
                s = b'a'
                filedata = b''
                with open(self.geonames_file, 'rb') as file :
                  filedata = file.read()
                self.human_names = []
                
                # take human names from names.txt
                with open(self.script_dir + "TR_human_names/human_names.txt", 'r') as file :
                  for f in file:
                      self.human_names.append(f.replace('\n',''))

                with open(self.script_dir + "TR_human_names/homonymic_words.txt", 'r') as file :
                  for f in file:
                      f = f.replace('\n','')
                      
                      if len(f) < 5:
                            f = f.lower()
                            f = f.replace('i̇','i')
                            f = f.replace('i̇','i')
                            self.not_p_names_s.append(f.capitalize())
                            
                      else:
                            f = f.lower()
                            f = f.replace('i̇','i')
                            f = f.replace('i̇','i')
                            self.not_p_names_l.append(f.capitalize())
                            
                # Replace the target string

                filedata = filedata.replace(b'#', b'---')
                self.text = filedata
                self.text = self.text.splitlines()   
                self.load_geonames_with_length(self.geonames_file)
                self.short_placelist2 = []
                self.long_placelist2 = []
                
                # this part will be modified again
                self.not_place_names_long_length = []           
                self.not_place_names_short_length = []
                
                for name in self.short_placelist:
                    if name not in self.not_p_names_s:
                            self.short_placelist2.append(name)  
                for name in self.long_placelist:
                    if name not in self.not_p_names_l:
                            self.long_placelist2.append(name)
              
                self.short_placelist = [name for name in self.short_placelist2]
                self.long_placelist = [name for name in self.long_placelist2]
                print(len(self.long_placelist2))
                self.short_place_names = self.construct_regex_based_placelist_grammar_s(self.short_placelist)
                self.long_place_names = self.construct_regex_based_placelist_grammar_l(self.long_placelist)
                self.human_names = self.construct_regex_based_placelist_grammar_human_names(self.human_names)
                self.not_single = self.construct_regex_based_placelist_grammar_human_names(self.not_p_names_l)
                #print("Number of place names with length smaller than 5: ", len(self.short_placelist2))
                #print("Number of place names with length 5 and larger than 5: ", len(self.long_placelist2))
                # combine place names with space character for human name concatination 
                self.long_place_names_wt_space = self.construct_regex_based_placelist_grammar_l_space(self.long_placelist)
              
                self.arr = []
                self.toks = []
                
                self.wrong_normalized_words = {}     
                self.words_with_adjacent_letters = []
                #print(len(self.words_with_adjacent_letters))
            
         def load_geonames_with_length(self, name_list):
                dt = np.dtype([('geonameid', '|i4'), ('pname', '|S30'), ('asciiname', '|S30'), ('alternatenames', '|S300'), \
                 ('latitude', '|S30'), \
                 ('longitude', '|S30'), ('feature_class', '|S30'), ('feature_code', '|S30'), ('country_code', '|S30'), ('cc2', '|S30'), \
                 ('admin1_code', '|S30'), ('admin2_code', '|S30'), ('admin3_code', '|S30'), ('admin4_code', '|S30'), ('population', '|S30'),\
                 ('elevation', '|S30'), ('dem', '|S30'), ('time_zone', '|S30'), ('modification_date', '|S30')])
               
                matrixCsv = np.genfromtxt(self.text, dtype=dt, delimiter='\t') 
                all_pnames_list1 = [pnames.decode('utf-8', errors='ignore') for pnames in matrixCsv['pname']]
                #print(all_pnames_list1)
                all_pnames_list2 = [pnames.decode('utf-8', errors='ignore') for pnames in matrixCsv['asciiname']]
                #all_pnames_list1 = [pnames.decode('utf-8') for pnames in matrixCsv['pname']]
                #all_pnames_list2 = [pnames.decode('utf-8') for pnames in matrixCsv['asciiname']]
               
                all_pnames_list = []
                for name in all_pnames_list1:
                     all_pnames_list.append(name)
                for name in all_pnames_list2:
                     all_pnames_list.append(name)
                #all_pnames_list_total =all_pnames_list1 + all_pnames_list2
                all_pnames_list_total = list(set(all_pnames_list))
                
                for i in all_pnames_list_total:
                    hsnw.write(i + '\n')
                # separate place names from the characters following '&' 
                for all_n in all_pnames_list:
                   if '&' in all_n:
                      indx = all_n.index('&')
                      index_ = all_pnames_list.index(all_n)
                      all_pnames_list.pop(index_)
                      new_word = all_n[:indx]
                      if new_word[-1] == ' ':
                         new_word = new_word[:-1]
                      all_pnames_list.append(new_word)
                      
                #print(all_pnames_list[:3])
                
                all_pnames_list = [pn for pn in all_pnames_list if len(pn)>2]
                all_pnames_list = [pn for pn in all_pnames_list]
                # separete place names according to their length
                self.short_placelist = list(filter(lambda x: len(x)<5, all_pnames_list))
                self.long_placelist = list(filter(lambda x: len(x)>=5, all_pnames_list))
                #self.short_placelist = [name for name in self.short_placelist]
                #self.long_placelist = [name for name in self.long_placelist]
                end = time.time()
                
                print(end-self.start)
                #print("Number of place names from geonames:",len(all_pnames_list))
                #print("Number of place names with length smaller than 5: ", len(self.short_placelist))
                #print("Number of place names with length 5 and larger than 5: ", len(self.long_placelist))
         
   
         def get_caseless_word_literal(self, word):
                # let the words be casefree             
                if isinstance(word, str):
                        return WordStart() + CaselessLiteral(word) + WordEnd()
                elif isinstance(word, list):
                        return WordStart() +oneOf(word, caseless=True) + WordEnd()
                else:
	                raise Exception("Literals should be type of strings or list of the strings. Given type:" + type(word))

            

         def get_regex_str_from_list(self, mylist,before_word, after_word):
                #Sentence boundary control with \\b does not allow place names to start with apostrophe.
                #Therefore, we do not use them together.
                rgx_str = ""
                for i,item in enumerate(mylist):
                    if len(item)>0:
                        if item[0] == "'":
                             rgx_str += after_word+"|"+before_word+item   
                        else:
                                if i == 0: # do not allow duplicate
                                        rgx_str += item 
                                else:
                                    rgx_str += after_word+"|"+before_word+item
                rgx_str += before_word
                return rgx_str  

         def replace_characters_and_sort_list(self, mylist):
                use_wlist = list(set(mylist))
                use_wlist = sorted(use_wlist, key=len, reverse=True)
                use_wlist = [tx.replace('.','\.') for tx in use_wlist] # not to match anything with . (dot)
                use_wlist = [tx.replace("(","\(") for tx in use_wlist] # not to match anything with . (dot)
                use_wlist = [tx.replace(")","\)") for tx in use_wlist] # not to match anything with . (dot)
                return use_wlist
                
         def get_regex_length_sorted_list_for_short(self,mylist):
                use_wlist = self.replace_characters_and_sort_list(mylist)
                return re.compile(self.get_regex_str_from_list(use_wlist,"\\b","\\b"), re.I)
                
         def get_regex_length_sorted_list_for_human(self,mylist):
                use_wlist = self.replace_characters_and_sort_list(mylist)
                return re.compile(self.get_regex_str_from_list(use_wlist,"\\b",""), re.I) 
                    
         def get_regex_length_sorted_list_for_long(self,mylist):
                use_wlist = self.replace_characters_and_sort_list(mylist)
                return re.compile(self.get_regex_str_from_list(use_wlist,"\\b", ""), re.I)
                
         def get_regex_length_sorted_list_for_long_space(self,mylist):
                use_wlist = self.replace_characters_and_sort_list(mylist)
                return re.compile(self.get_regex_str_from_list(use_wlist,"\\s",""), re.I)
                
         def construct_regex_based_placelist_grammar_l(self,lng_plist):
                wlist_ptrn_long = self.get_regex_length_sorted_list_for_long(lng_plist)
                return Combine(Regex(wlist_ptrn_long))
                
         def construct_regex_based_placelist_grammar_l_space(self,lng_plist):
                wlist_ptrn_long = self.get_regex_length_sorted_list_for_long_space(lng_plist)
                return Combine(Regex(wlist_ptrn_long))
                
         def construct_regex_based_placelist_grammar_human_names(self,human_name_list):
                wlist_ptrn_long = self.get_regex_length_sorted_list_for_human(human_name_list)
                return Combine(Regex(wlist_ptrn_long)) 
                      
         def construct_regex_based_placelist_grammar_s(self,shrt_plist):
                wlist_ptrn_short = self.get_regex_length_sorted_list_for_short(shrt_plist)
                return Combine(Regex(wlist_ptrn_short))
          
         
         def normalize_text(self, text):
                # use a generator expression to join words for memory safe
                # make words plain by groupby -----> floooowwwwweeeer -> flower
                text = ''.join(k for k, g in groupby(text))
                
                text_ = text
                # seperate original words like "le'tt'er" to prevent a wrong choice
                for key,value in self.wrong_normalized_words.items():
                    if key in text:
                       text_ = text_.replace(key, value)
                       break
                return text_
                
                        
         def lower_words(self, word):
                """ make the word lower and escape ascii characters """
                word = word[0].encode('ascii', 'ignore')
                self.unicode_word = unicode_tr(word)
                return self.unicode_word.lower()
         
               
         def normalize_text_for_dict(self, text):
                # use a generator expression to join words for memory safe
                # make words plain by groupby -----> floooowwwwweeeer -> flower
                text = ''.join(k for k, g in groupby(text))
                return text

         def appendix_regex_front(self, appendix_array):
               appendix_pttrn = "\s|\'|"
               for i in range(len(appendix_array)):
                   appendix_pttrn_ = appendix_array[i] + '(' + '\s' + '|' + 'de' + '\s' +'|' + 'da' + '\s' +'|' +  'deki' + '\s' + \
                          '|' + 'daki' + '\s' + ')'
                   if i != (len(appendix_array)-1):
                         appendix_pttrn += appendix_pttrn_+ '|'
                   else:
                         appendix_pttrn += appendix_pttrn_ 
               return appendix_pttrn

         def appendix_regex_back(self, appendix_array):
               appendix_pttrn = "" # 'dan(\s|da\s)|den(\s|de\s)
               for i in range(len(appendix_array)):
                   appendix_pttrn_ = appendix_array[i] + '(' + '\s' + '|' + 'da' + '\s' + ')'
                   if i != (len(appendix_array)-1):
                         appendix_pttrn += appendix_pttrn_ + '|'
                   else:
                         appendix_pttrn += appendix_pttrn_ + '|,|\'|.|#|@|-|\"|]|)'
               return appendix_pttrn

         def location_supporter_regex(self, location_supporters):
               location_supporter_pttrn = ""
               for i in range(len(location_supporters)):
                   if i != (len(location_supporters)-1):
                      location_supporter_pttrn += '\s' + location_supporters[i] + '|'
                   else: 
                      location_supporter_pttrn += '\s' + location_supporters[i]
                   
               return re.compile(location_supporter_pttrn, re.I)
 
         def construct_location_grammar(self):
                location_pttrn_s = Combine(Optional("#|(")+self.short_place_names+\
                    FollowedBy(Regex(self.appendix_pattern))+Optional("#|)")).setResultsName("shortlengthplace") 
                
                location_pttrn_l = Combine(Optional("#|(")+self.long_place_names +\
                     FollowedBy(Regex(self.appendix_pattern))+Optional("#|)")).setResultsName("longlengthplace")
                human_n = self.human_names.setResultsName("human_names")
                long_pnames_with_space = Combine(self.long_place_names_wt_space + \
                            FollowedBy(Regex(self.appendix_pattern))).setResultsName("space")
                
                human_n = Combine(OneOrMore(human_n)+long_pnames_with_space) 
                human_n2 = Combine(Optional("#")+self.long_place_names+\
                        OneOrMore(Regex(self.location_supporter_pattern))+Optional("-"))
                single = Combine(Optional("#")+self.not_single+\
                        OneOrMore(Regex(self.location_supporter_pattern))+Optional("-"))
                
                end = time.time()
                #print('regex3',end-self.start)
                self.toks = []
                def location_pttrn_s_trnsfrm(loc,toks):
                      return {"location":"".join(toks["shortlengthplace"])}
                      
                def location_pttrn_l_trnsfrm(loc,toks):
                      return {"location":"".join(toks["longlengthplace"])}
                      
                def location_pttrn2_trnsfrm(loc,toks):
                      return {"location":"".join(toks)}
                      
                def human(loc,toks):
                      return {"human":"".join(toks)}    
                      
                location_pttrn_s.setParseAction(location_pttrn_s_trnsfrm)
                location_pttrn_l.setParseAction(location_pttrn_l_trnsfrm)
                #location_pttrn2.setParseAction(location_pttrn2_trnsfrm)
                human_n.setParseAction(human)
                human_n2.setParseAction(human)
                single.setParseAction(location_pttrn2_trnsfrm)
                location_pttrn = single | human_n  | location_pttrn_l | location_pttrn_s 
              
                end = time.time()
                #print('regex4',end-self.start)
                return location_pttrn 
           
         
