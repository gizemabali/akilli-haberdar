#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from pyparsing import Word, alphas, nums, WordStart, WordEnd, Literal, Optional, SkipTo, ParseResults, CaselessLiteral,\
        OneOrMore, Group, White, ZeroOrMore, NotAny, Suppress, oneOf, Combine, Regex, lineStart, Empty, locatedExpr, ParseException, LineStart
import ast
import codecs
import os
import pandas as pd
import json
import sys
import pymongo
from pymongo import MongoClient
import numpy as np
from pathlib import Path
from collections import Counter
sys.path.append('/home/hp/Masaüstü/analysing_aegean_tweets/name_entity_recognation/ner/organization_recognizer/')
from organization_recognizer_tr import OrganizationNames
sys.path.append('/home/hp/Masaüstü/analysing_aegean_tweets/name_entity_recognation/ner/human_name_recognizer/')
from human_names_recognizer import HumanNames
from datetime import datetime


class Get_Info():
       """This script finds hashtags and tweets which have no hashtag,
          stores them into seperate arrays and then it searchs for hashtags on tweets
          it uses July 16 dataset and results are at the bottom of this script!"""
       def __init__(self):
            self.db_host = 'localhost'
            self.db_port = 27017
            self.dbname = 'tweets_aegaen'
            self.collname = 'tweets_aegaen'
          
            try:
                self.client = MongoClient(self.db_host, self.db_port)
                self.localdb = self.client[self.dbname]
                self.localcol = self.localdb[self.collname]
                print("MongoDB database : " + self.dbname + ", collection : " + self.collname + ")")

            except:
                print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")

            current_time = str(datetime.now())
            current_time_db = str(datetime.now())
            #print(current_time.split(" "))
            current_day = str(current_time.split(" ")[0])
            date = current_day.split("-")[:2]
            day = current_day.split("-")[2]
            print(current_day)
            hour = int(current_time.split(" ")[1].split(':')[0])
            yesterday = str(int(day) - 1)
            if len(yesterday) == 1:
                 yesterday = "0"+str(yesterday)
            yesterday = "-".join(date) + "-" + str(yesterday)
            print(yesterday)
            minutes = current_time.split(" ")[1].split(':')[1]
            seconds = current_time.split(" ")[1].split(':')[2].split('.')[0]
            self.all_tweets_from_db_count = self.localcol.count({'created_at':{'$gte': { '$date': yesterday+"T00:00:00.001Z"},'$lte': {'$date':yesterday+"T23:59:59.000Z"}}})

            self.collname2 = 'aegaen_tweets'
            try:
                
                self.localcol2 = self.localdb[self.collname2]
                print("MongoDB database : " + self.dbname + ", collection : " + self.collname + ")")

            except:
                print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
            self.localcol2.insert({'day': yesterday, 'count': self.all_tweets_from_db_count})


Get_Info()
