import json
import pymongo
from pymongo import MongoClient


def Connect(collname):
      db_host = 'localhost'
      db_port = 27017
      dbname = 'hashtags'
      collname = collname

      try:
          client = MongoClient(db_host, db_port)
          localdb = client[dbname]
          localcol = localdb[collname]
          print("MongoDB database : " + dbname + ", collection : " + collname + ")")

      except:
          print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
      return localcol

idi = 1
with open('/home/hp/social_media_analysis/part2/sample_tweets2/related_tweets.txt','rU') as f:
    localcol = Connect('golden_set')
    red = f.read()
    line = red.split('*****')
    tweet = ''
    username = ''
    user_screenname = ''
    date = ''
    for l in line:
       try:
           l = l.split('-----')
           tweet = l[0]
           username = l[1]
           user_screenname = l[2]
           date = l[3]
       except:
           print(l)
       
       if l != '':
          #localcol.insert({'type': 'golden_set', 'tweet_text':tweet, 'username': username, 'user_screenname':user_screenname, 'date':date})
          localcol.insert({"_id":{"oid":'G'+str(idi)},"source":"","user":{"profile_use_background_image":"true","friends_count":0,"statuses_count":0,"profile_image_url_https":"","profile_banner_url":"","screen_name":user_screenname,
"created_at":"","followers_count":637,"name":username,"geo_enabled":"true","profile_text_color":"333333","default_profile":"true","profile_link_color":"1DA1F2","favourites_count":608,"location":"","profile_sidebar_fill_color":"DDEEF6","profile_image_url":"","profile_sidebar_border_color":"C0DEED","profile_background_color":"F5F8FA","description":"","id":{"numberLong":""},"id_str":"","lang":"tr"},"entities":{"media":[{"type":"photo","media_url_https":"","display_url":"","expanded_url":"","id":{"numberLong":""},"sizes":{"large":{"w":720,"h":1280,"resize":"fit"},"medium":{"w":600,"h":1067,"resize":"fit"},"small":{"w":340,"h":604,"resize":"fit"},"thumb":{"w":150,"h":150,"resize":"crop"}},"url":"","media_url":"","id_str":"","indices":[40,63]}]},"filter_level":"low","place":{"place_type":"city","country_code":"TR","country":"Türkiye","bounding_box":{"coordinates":[[]],"type":"Polygon"},"id":"","name":"","full_name":"","url":""},"text":tweet,"created_at":{"date":date},"display_text_range":[0,39],"id":{"numberLong":""},"extended_entities":{"media":[{"video_info":{"variants":[{"url":"","bitrate":832000,"content_type":"video/mp4"},{"content_type":"application/dash+xml","url":""},{"url":"","bitrate":2176000,"content_type":"video/mp4"},{"content_type":"","url":""},{"url":"","bitrate":320000,"content_type":"video/mp4"}],"aspect_ratio":[9,16],"duration_millis":42154},"type":"video","media_url_https":"","display_url":"","expanded_url":"","id":{"numberLong":"815345858430562304"},"sizes":{"large":{"w":720,"h":1280,"resize":"fit"},"medium":{"w":600,"h":1067,"resize":"fit"},"small":{"w":340,"h":604,"resize":"fit"},"thumb":{"w":150,"h":150,"resize":"crop"}},"url":"","media_url":"","id_str":"","indices":[40,63]}]},"id_str":"815347090113097728","timestamp_ms":"1483228869356","lang":"tr", 'type':'golden_set'}
)
          idi += 1

"""
idi = 1
with open('/home/hp/social_media_analysis/part2/sample_tweets2/not_related_tweets.txt','rU') as f:
    localcol = Connect('not_related_set')
    red = f.read()
    line = red.split('*****')
    for l in line:
       if l != '':
          #localcol.insert({'type':'not_related', 'tweet_text':l})
          localcol.insert({"_id":{"oid":'N'+str(idi)},"source":"","user":{"profile_use_background_image":"true","friends_count":0,"statuses_count":0,"profile_image_url_https":"","profile_banner_url":"","screen_name":"","created_at":"","followers_count":637,"name":"","geo_enabled":"true","profile_text_color":"333333","default_profile":"true","profile_link_color":"1DA1F2","favourites_count":608,"location":"","profile_sidebar_fill_color":"DDEEF6","profile_image_url":"","profile_sidebar_border_color":"C0DEED","profile_background_color":"F5F8FA","description":"","id":{"numberLong":""},"id_str":"","lang":"tr"},"entities":{"media":[{"type":"photo","media_url_https":"","display_url":"","expanded_url":"","id":{"numberLong":""},"sizes":{"large":{"w":720,"h":1280,"resize":"fit"},"medium":{"w":600,"h":1067,"resize":"fit"},"small":{"w":340,"h":604,"resize":"fit"},"thumb":{"w":150,"h":150,"resize":"crop"}},"url":"","media_url":"","id_str":"","indices":[40,63]}]},"filter_level":"low","place":{"place_type":"city","country_code":"TR","country":"Türkiye","bounding_box":{"coordinates":[[]],"type":"Polygon"},"id":"","name":"","full_name":"","url":""},"text":l,"created_at":{"date":""},"display_text_range":[0,39],"id":{"numberLong":""},"extended_entities":{"media":[{"video_info":{"variants":[{"url":"","bitrate":832000,"content_type":"video/mp4"},{"content_type":"application/dash+xml","url":""},{"url":"","bitrate":2176000,"content_type":"video/mp4"},{"content_type":"","url":""},{"url":"","bitrate":320000,"content_type":"video/mp4"}],"aspect_ratio":[9,16],"duration_millis":42154},"type":"video","media_url_https":"","display_url":"","expanded_url":"","id":{"numberLong":"815345858430562304"},"sizes":{"large":{"w":720,"h":1280,"resize":"fit"},"medium":{"w":600,"h":1067,"resize":"fit"},"small":{"w":340,"h":604,"resize":"fit"},"thumb":{"w":150,"h":150,"resize":"crop"}},"url":"","media_url":"","id_str":"","indices":[40,63]}]},"id_str":"815347090113097728","timestamp_ms":"1483228869356","lang":"tr", 'type':'not_related'})
          idi += 1
   


idi = 1
with open('/home/hp/social_media_analysis/part2/sample_tweets/test.txt','rU') as f:
    localcol = Connect('learned_set')
    red = f.read()
    line = red.split('-------')
    for l in line:
        if l != '':
           tweet = l.split('***')[0]
           status = l.split('***')[1].replace('\n','')
           
           if str(status) == "related":
              print(tweet, status)
              #localcol.insert({'learned_set':tweet})
              localcol.insert({"_id":{"oid":"L"+str(idi)},"source":"","user":{"profile_use_background_image":"true","friends_count":0,"statuses_count":0,"profile_image_url_https":"","profile_banner_url":"","screen_name":"None",
"created_at":"","followers_count":637,"name":"None","geo_enabled":"true","profile_text_color":"333333","default_profile":"true","profile_link_color":"1DA1F2","favourites_count":608,"location":"","profile_sidebar_fill_color":"DDEEF6","profile_image_url":"","profile_sidebar_border_color":"C0DEED","profile_background_color":"F5F8FA","description":"","id":{"numberLong":""},"id_str":"","lang":"tr"},"entities":{"media":[{"type":"photo","media_url_https":"","display_url":"","expanded_url":"","id":{"numberLong":""},"sizes":{"large":{"w":720,"h":1280,"resize":"fit"},"medium":{"w":600,"h":1067,"resize":"fit"},"small":{"w":340,"h":604,"resize":"fit"},"thumb":{"w":150,"h":150,"resize":"crop"}},"url":"","media_url":"","id_str":"","indices":[40,63]}]},"filter_level":"low","place":{"place_type":"city","country_code":"TR","country":"Türkiye","bounding_box":{"coordinates":[[]],"type":"Polygon"},"id":"","name":"","full_name":"","url":""},"text":tweet,"created_at":{"date":""},"display_text_range":[0,39],"id":{"numberLong":""},"extended_entities":{"media":[{"video_info":{"variants":[{"url":"","bitrate":832000,"content_type":"video/mp4"},{"content_type":"application/dash+xml","url":""},{"url":"","bitrate":2176000,"content_type":"video/mp4"},{"content_type":"","url":""},{"url":"","bitrate":320000,"content_type":"video/mp4"}],"aspect_ratio":[9,16],"duration_millis":42154},"type":"video","media_url_https":"","display_url":"","expanded_url":"","id":{"numberLong":"815345858430562304"},"sizes":{"large":{"w":720,"h":1280,"resize":"fit"},"medium":{"w":600,"h":1067,"resize":"fit"},"small":{"w":340,"h":604,"resize":"fit"},"thumb":{"w":150,"h":150,"resize":"crop"}},"url":"","media_url":"","id_str":"","indices":[40,63]}]},"id_str":"815347090113097728","timestamp_ms":"1483228869356","lang":"tr", 'type':status}
)
           idi += 1
"""

