#!/home/user/anaconda3/bin/python3.5
import sys
sys.path.insert(1, r'../datautils/')  # add to pythonpath
from tweepy import OAuthHandler
from tweepy import Stream
import configparser
import tweepy
from tweepy.streaming import StreamListener
import json

import statistics as s
import pandas as pd
import importlib
import json
import pymongo
from pymongo import MongoClient
import smtplib
from datetime import datetime
import logging

logging.basicConfig(
		format='%(asctime)s, %(levelname)s: %(message)s',
		filename='musma.log',
		datefmt='%d-%m-%Y, %H:%M',
		level=logging.INFO)


db_host = 'localhost'
db_port = 27017
dbname = 'hashtags'
collname = 'counts2'


client = MongoClient(db_host, db_port)

localdb = client[dbname]
localcol = localdb[collname]
print("MongoDB database : " + dbname + ", collection : " + collname + ")")

with open("/home/hp/Masaüstü/aegaen_tweets_counts.json",'rU') as f:
       for line in f:
           line = line.replace("$","")
           line = json.loads(line)
           localcol.insert(line)
           
