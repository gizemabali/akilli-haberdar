#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import pandas as pd
import json
import sys
import pymongo
from pymongo import MongoClient
import numpy as np
from pathlib import Path
from collections import Counter

from datetime import datetime


class Get_Info():
       """This script finds hashtags and tweets which have no hashtag,
          stores them into seperate arrays and then it searchs for hashtags on tweets
          it uses July 16 dataset and results are at the bottom of this script!"""
       def __init__(self):
            self.db_host = 'localhost'
            self.db_port = 27017
            self.dbname = 'tweets_aegaen'
            self.collname = 'tweets_aegaen'
            current_time = str(datetime.now())
            current_time_db = str(datetime.now())
            #print(current_time.split(" "))
            current_day = str(current_time.split(" ")[0])
            date = current_day.split("-")
            year = int(date[0])
            month = int(date[1])
            day = int(date[2])
            day_ago = day - 1
            
            
            try:
                self.client = MongoClient(self.db_host, self.db_port)
                self.localdb = self.client[self.dbname]
                self.localcol = self.localdb[self.collname]
                print("MongoDB database : " + self.dbname + ", collection : " + self.collname + ")")

            except:
                print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")

            self.collname2 = 'tweets_aegaen_counts'
            try:
                
                    self.localcol2 = self.localdb[self.collname2]
                    print("MongoDB database : " + self.dbname + ", collection : " + self.collname2 + ")")

            except:
                    print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
         
          
            # convert your date string to datetime object
            start = datetime(year, month, day_ago, 0, 0, 0)
            end = datetime(year, month, day_ago, 23, 59, 59)
            self.all_tweets_from_db_count = self.localcol.count({'created_at':{'$gte': start,'$lte': end}})

            print(self.all_tweets_from_db_count)
            self.localcol2.insert({'date': current_day, 'count': self.all_tweets_from_db_count})
            

Get_Info()

