#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from pyparsing import Word, alphas, nums, WordStart, WordEnd, Literal, Optional, SkipTo, ParseResults, CaselessLiteral,\
        OneOrMore, Group, White, ZeroOrMore, NotAny, Suppress, oneOf, Combine, Regex, lineStart, Empty, locatedExpr, ParseException, LineStart
import ast
import codecs
import os
import pandas as pd
import json
import sys
import pymongo
from pymongo import MongoClient
import numpy as np
from pathlib import Path
from collections import Counter
sys.path.append('/home/hp/Masaüstü/adsiz/analysing_aegean_tweets/name_entity_recognation/ner/organization_recognizer/')
from organization_recognizer_tr import OrganizationNames
sys.path.append('/home/hp/Masaüstü/adsiz/analysing_aegean_tweets/name_entity_recognation/ner/human_name_recognizer/')
from human_names_recognizer import HumanNames
from datetime import datetime

organization_reg = OrganizationNames()
human_reg = HumanNames()

sample_text_file = open('tweets_and_hashtags.txt','w')
class Get_Info():
       """This script finds hashtags and tweets which have no hashtag,
          stores them into seperate arrays and then it searchs for hashtags on tweets
          it uses July 16 dataset and results are at the bottom of this script!"""
       def __init__(self):
            self.db_host = 'localhost'
            self.db_port = 27017
            self.dbname = 'tweet_data'
            self.collname = 'aegaen_tweets'
            self.collname_trend = 'trend_topics'
            self.organization_recog = organization_reg.construct_location_grammar()
            self.human_recog = human_reg.construct_location_grammar()
            try:
                self.client = MongoClient(self.db_host, self.db_port)
                self.localdb = self.client[self.dbname]
                self.localcol = self.localdb[self.collname]
                print("MongoDB database : " + self.dbname + ", collection : " + self.collname + ")")
                self.localcol_trend = self.localdb[self.collname_trend]
            except:
                print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
            self.localcol.drop()
            current_time = str(datetime.now())
            #print(current_time.split(" "))
            current_day = current_time.split(" ")[0]
            year = int(current_day.split("-")[0])
            month = int(current_day.split("-")[1])
            day = int(current_day.split("-")[2])
            hour = int(current_time.split(" ")[1].split(':')[0])
            minute = int(current_time.split(" ")[1].split(':')[1])
            seconds = int(current_time.split(" ")[1].split(':')[2].split('.')[0])
            #hour_ago = hour - 12
            #print(hour_ago)
            #start = datetime(year, month, day, hour_ago, minute, seconds)
            #end = datetime(year, month, day, hour, minute, seconds)
            #all_tweets_from_db = self.localcol.find({'created_at':{'$gte': start,'$lte': end}})
            self.all_tweets_from_db = []
            with open("/home/hp/json_kasim/kasim06.json",'rU') as f:
               for line in f:
                   line = line.replace("$","")
                   line = json.loads(line)
                   self.all_tweets_from_db.append(line)
            self.all_hashtags = [] ##store every hashtag
            self.no_hashtag_tweets = [] # store tweets that have no hashtag
            self.hashtag_tweets = [] # store tweets that have hashtags
            self.hashtags = "" 
            self.unsecure_username_arr = []
            self.adult_hashtags = ['adult','grupseks','antalyasex','evlicift','swingers','CUCKOLDONUR','erotic','porno','MASOR', \
                                    'masaj','antalyacift','erotic','fantazi','sex','seks']

            self.hashtags_arr = [] #store hashtags which are longer than 5 characters to insert white space
            self.get_tweets(self.all_tweets_from_db)
            self.findall_pttrn = self.findall_pattern(self.hashtags_arr)
            self.total_hashtags = [] 
            
            
            self.search_hashtags_in_tweets_with_no_hashtag(self.no_hashtag_tweets)
            #count after searching on tweets which have no hashtag
            counts = Counter(self.all_hashtags)
            count = counts.most_common(150)
            self.localcol_trend.insert({'all':'all', 'counts': count})
            """
            for hashtag,count in count[:]:
                print(hashtag, count)
            
            """
            

       def findall_pattern(self, all_hashtags):
            pattern = ''
            all_hashtags = list(set(all_hashtags))
            all_hashtags = sorted(all_hashtags, key=len, reverse=True)
            for hashtag in all_hashtags:
               if all_hashtags.index(hashtag)==0:
                  if len(hashtag) > 5:
                    pattern = self.insert_newline_to_words(hashtag)
                  else:
                    pattern = hashtag
               else:
                  if len(hashtag) > 5:
                    pattern = pattern + '|' + self.insert_newline_to_words(hashtag)
                  else:
                    pattern = pattern + '|' + hashtag
            
            return re.compile(pattern, re.I)
       
       def search_hashtags_in_tweets_with_no_hashtag(self, tweets):
           hashtag_loc = self.construct_location_grammar()
           
           for tweet in tweets:
               old_tweet = tweet['tweet_text']
               tweet_str_id = tweet['id']['numberLong']
               username = tweet['username']
               user_screen_name = tweet['user_screenname']
               media = tweet['source']
               user_image_link = tweet['user_image']
               tweet = old_tweet.split(" ")
               new_tweet = []
               for word in tweet:
                   if word != '':
                      if word[0] != '@':
                       new_tweet.append(word) 
               hashtag_split = re.findall(self.findall_pttrn, ' '.join(new_tweet))
               hashtag_split = list(set(hashtag_split))
               
               for hashtags in hashtag_split:
                   #for hashtag in hashtags:
                   
                   if hashtags != '':
                       try:
                          starting_index = old_tweet.index(hashtags)
                          ending_index = starting_index + len(hashtags)
                       except:
                          starting_index = 0
                          ending_index = 0
                       hashtags = hashtags.lower()
                       hashtags = hashtags.replace('i̇','i')
                       hashtags = hashtags.replace(" ","")
                       self.all_hashtags.append(hashtags)
                      
                       self.localcol_trend.insert({'start':starting_index,'end':ending_index,'hashtag':hashtags,'tweet_text':old_tweet, \
                          'username': username, 'user_screenname':user_screen_name, 'source': media, 'user_image':user_image_link, 'tweet_str_id': tweet_str_id})
                       #print(hashtags)

       def insert_newline_to_words(self, word): ##insert white space btw every letter
            new_word = ''
            for i,letter in enumerate(word):
               if i == 0:
                 new_word = letter
               else:
                 new_word = new_word + '\s*' + letter
            return new_word


       def get_regex(self, hashtags):
           rgx_str = "\\b"
           for i,item in enumerate(hashtags):
              try:
                 if item[0] == "'":
                     rgx_str += "|\\b"+ str(item)
                 else:
                     if i == 0: # do not allow duplicate
                         rgx_str += str(item)
                     else:
                         rgx_str += "|\\b"+ str(item)
              except:
                 pass  
           return rgx_str
            
       def get_regex_sorted(self):
           setted = list(set(self.all_hashtags))
           hashtags = sorted(setted, key=len, reverse=True)
           #print("2")
           hashtags = [hashtag.replace('.','\.') for hashtag in hashtags] # not to match anything with . (dot)
           hashtags = [hashtag.replace("(","\(") for hashtag in hashtags] # not to match anything with . (dot)
           hashtags = [hashtag.replace(")","\)") for hashtag in hashtags] # not to match anything with . (dot)
           rgex = "1"
           #print("3")
           #print(len(rgex))
           rgex_compiled = re.compile(self.get_regex(hashtags), re.I)
           return Combine(Regex(rgex_compiled))
       
       def construct_location_grammar(self):
           hashtag_pttrn = self.hashtags.setResultsName("hashtag")
           self.toks = []
           def hashtag_pttrn_confirmed(loc,toks):
                  return toks["hashtag"].lower()
                  
           hashtag_pttrn.setParseAction(hashtag_pttrn_confirmed)
           return hashtag_pttrn

       def use_tweets_except_FI(self, tweet, media):
           try: 
             user_name = tweet['user']['name']
             user_screen_name = tweet['user']['screen_name']
             source = media
             tweet_str_id = tweet['id']['numberLong']
             user_image_link = tweet['user']['profile_image_url_https']
             no_hashtag_keys = []
             all_hashtags_on_tweet = tweet['entities']['hashtags']
            
             if tweet['user']['screen_name'] not in self.unsecure_username_arr:    
                
                self.localcol.insert({'all_tweets': 'all_tweets', 'tweet_text': tweet['text'], 'username': user_name,\
                     'user_screenname':user_screen_name, 'source': media, 'user_image':user_image_link, 'tweet_str_id': tweet_str_id})

                for hashtag in all_hashtags_on_tweet:
                  new_sentence = []
                  ## extract tweets which are +18
                  starting_index = hashtag['indices'][0]
                  ending_index = hashtag['indices'][1]
                  lowed_hashtag = hashtag['text'].lower()
                  lowed_hashtag = lowed_hashtag.replace('i̇','i')
                  lowed_hashtag = lowed_hashtag.replace('i̇','i')
                  if 'i̇' in lowed_hashtag:
                       lowed_hashtag = lowed_hashtag.replace('i̇','i')
                  self.all_hashtags.append(lowed_hashtag) ##append all hashtags
                  
                  self.localcol_trend.insert({'start':starting_index,'end':ending_index,'hashtag':lowed_hashtag,'tweet_text':tweet['text'], \
                      'username': user_name, 'user_screenname':user_screen_name, 'source': media, 'user_image':user_image_link, 'tweet_str_id': tweet_str_id})
                  ##take hashtags that are longer than 5 characters to insert white spaces btw every letter 
                  ##so that it can find if hashtag is 'sokağasakınçıkma' --> then 'sokağa sakın çıkmayın'
                  
                  category_boolean = self.check_hashtag_category(hashtag['text'])
                  #if len(hashtag['text'])>5: 
                  if category_boolean: 
                     lowed_hashtag = hashtag['text'].lower()
                     self.hashtags_arr.append(lowed_hashtag.replace('i̇','i')) 

           except:
               tweet_str_id = tweet['id']['numberLong']
               user_name = tweet['user']['name']
               user_screen_name = tweet['user']['screen_name']
               source = media
               user_image_link = tweet['user']['profile_image_url_https']
               
               if tweet['user']['screen_name'] not in self.unsecure_username_arr:
                  self.localcol.insert({'all_tweets': 'all_tweets', 'tweet_text': tweet['text'], 'username': user_name,\
                     'user_screenname':user_screen_name, 'source': media, 'user_image':user_image_link, 'tweet_str_id': tweet_str_id})
                  self.no_hashtag_tweets.append({'tweet_text': tweet['text'], 'username': user_name, 'user_screenname':user_screen_name,\
                     'source': media, 'user_image':user_image_link, 'id': {'numberLong':tweet_str_id}}) ##take tweets with no hashtag


       def check_hashtag_category(self, hashtag):
            organization = self.organization_recog.searchString(hashtag)
            human_name = self.human_recog.searchString(hashtag)
            
            if len(organization) != 0 or len(human_name) != 0:
                  print(organization, human_name)
                  return True
            return False

       def get_tweets(self, tweet_arr):
            # get hashtags and tweettext
            i = 0
            arr = []
            unsecure_usernames = []
            for line in tweet_arr:
                  for a_hash in self.adult_hashtags:
                      if a_hash in line['text']:
                          unsecure_usernames.append(line['user']['screen_name'])
                          break
            self.unsecure_username_arr = list(set(unsecure_usernames))

            for line in tweet_arr:
                  #line = json.loads(line)           
                  hashtags = []
                  source_link = line['source']
                  # the pattern that is used to check if the media is Foursquare
                  pttr = '(<a href=.* rel=\"nofollow\">)(.*)(</a>)'
                  source = re.search(pttr,source_link)
                  # extract the media information
                  media = source.group(2) 
                  if media != "Foursquare": # search on all media tweets except from Foursquare
                      self.use_tweets_except_FI(line, media)
                       
            #count before searching tweets which have no hashtag
            counts = Counter(self.all_hashtags)
            count = counts.most_common(150)
            """
            for hashtag,count in count[:]:
                print(hashtag, count)
                #pass
            """
            self.hashtags = self.get_regex_sorted()
           
        

Get_Info() 

