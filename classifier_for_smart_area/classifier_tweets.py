#!/usr/bin/env python
# -*- coding: utf-8 -*-

from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob
import json
import pymongo
import sys
from pymongo import MongoClient
from pygeocoder import Geocoder
from datetime import datetime
sys.path.append('/home/hp/Masaüstü/adsiz/analysing_aegean_tweets/name_entity_recognation/ner/organization_recognizer/')
from organization_recognizer_tr import OrganizationNames
sys.path.append('/home/hp/Masaüstü/adsiz/analysing_aegean_tweets/name_entity_recognation/ner/place_name_recognizer/')
from place_name_recognizer_tr import PlaceNames
db_host = 'localhost'
db_port = 27017
dbname = 'hashtags'
collname = 'learned_set'

try:
          client = MongoClient(db_host, db_port)
          localdb = client[dbname]
          localcol = localdb[collname]
          #print("MongoDB database : " + dbname + ", collection : " + collname + ")")

except:
          pass
hs = open("gizem.txt", "w")
class Train_Mechanism:

       def __init__(self):
             self.train = []
             self.get_related_set()
             self.get_not_related_set()
             self.all_tweets = []
             self.pn_reg3 = PlaceNames()
             self.location_recog = self.pn_reg3.construct_location_grammar()
             self.get_last_tweets()
             self.cl = NaiveBayesClassifier(self.train)
             self.train_tweets(self.all_tweets)

       def get_related_set(self):
            self.collname = 'golden_set'
            self.localcol = localdb[self.collname] 
            self.related = self.localcol.find()
            for tweet in self.related:
               self.train.append((tweet['text'].replace('\n',''), 'related'))


       def get_not_related_set(self):
            self.collname = 'not_related_set2'
            self.localcol = localdb[self.collname]
            self.not_related = self.localcol.find()
            for tweet in self.not_related:
               self.train.append((tweet['tweet_text'].replace('\n',''), 'not_related')) 

       def get_last_tweets(self):
            #dbname = 'aegaen_tweets'
            #collname = 'aegaen_tweets'
            dbname = 'hashtags'
            collname = 'new_learned'
            db_host = 'localhost'
            db_port = 27017
            try:
                client = MongoClient(db_host, db_port)
                localdb = client[dbname]
                localcol = localdb[collname]
                #print("MongoDB database : " + self.dbname + ", collection : " + self.collname + ")")

            except:
                #print("!!!!!!!! MONGODB CONNECTION FAILED !!!!!!!!")
                pass
            
            hour_indexes = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,0]
            ## get current time and split it
            current_time = str(datetime.now())
            print('current_time', current_time)
            #print(current_time.split(" "))
            current_day = current_time.split(" ")[0]
            year = int(current_day.split("-")[0])
            month = int(current_day.split("-")[1])
            day = int(current_day.split("-")[2])
            hour = int(current_time.split(" ")[1].split(':')[0])
            print('hour', hour)
            minute = int(current_time.split(" ")[1].split(':')[1])
            seconds = int(current_time.split(" ")[1].split(':')[2].split('.')[0])
            hour_ago = hour_indexes[hour_indexes.index(hour) - 5]
            print(hour_ago)
            start = datetime(year, month, day, hour_ago, minute, seconds)
            end = datetime(year, month, day, hour, minute, seconds)
            #all_tweets_from_db = localcol.find({'created_at':{'$gte': start,'$lte': end}})
            all_tweets_from_db = localcol.find()
            #{'created_at':{'$gte': start,'$lte': end}})
            for tweet in all_tweets_from_db:
                self.all_tweets.append(tweet)

       def train_tweets(self, tweets):
            db_host = 'localhost'
            db_port = 27017
            dbname = 'hashtags'
            collname = 'learned_set'
            try:
               client = MongoClient(db_host, db_port)
               localdb = client[dbname]
               localcol = localdb[collname]
               #print("MongoDB database : " + dbname + ", collection : " + collname + ")")
            except:
               pass
            collname = 'new_learned2_21'
            localcol = localdb[collname]
            collname2 = 'new_learned_text2_21'
            localcol2 = localdb[collname2]
            count = 0
            count2 = 0
             
            for tweet in tweets:
                count = count + 1
                
                #tweet_message = tweet['tweet_text'].replace("$","")
                classified = self.cl.classify(tweet['text'])
                #print(number, classified)
                if classified == 'related':
                     result = self.find_location(tweet)
                     if result != False:
                        localcol.insert(result)
                        print("location name")
                        #count2 = count2 + 1
                        #localcol2.insert({'text' : tweet_message, 'count_str' : str(count2)})
                     else:
                        print("no location name")
                        localcol2.insert({'text' : tweet})
                else:
                     print("passed")
       
    
       def find_location(self, tweet):
            r = []
            try:
                line = tweet['text']
                line = line.lower()
                sentence = line.replace('i̇','i')
                location = self.location_recog.searchString(sentence)
                location = list(set(location))
                locations = []
                for loc in location:
                  for k in loc[0].keys():
                     if k == 'location':
                        loca = loc[0]['location']
                        if loca != '' or loca != None:
                              locations.append(loca)
                locations = list(set(locations))
                print(locations)
                tweet['location_names'] = {'location':locations}
                try:
                   for loca in locations:
                                               
                           hs.write(loca+ '\n')
                           results = Geocoder.geocode(loca)
                           print(loca, results[0].coordinates)
                           r.append(results[0].coordinates)
                       
                   tweet['location_coordinates'] = {'location':r}
                   #tweet['location_names'] = {'location':str(location)}
                except:
                     pass
                
            except:
                  pass
           
            if location != []:
                   print(location)
                   
                   return tweet
            else:
                   return False
             
                  
Train_Mechanism()

